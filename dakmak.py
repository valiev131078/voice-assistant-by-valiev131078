import webbrowser
import sys
import pyttsx3
import speech_recognition
import datetime
#from pyowm import OWM
#from pyowm.utils import config as cfg


youtube = ['открой youtube', 'открыть youtube', 'открой мне youtube', 'youtube']
time = ['сколько времени', 'какой час', 'время', 'час', 'сколько время', 'какое время']
instagram = ['открой instagram', 'открыть instagram', 'открой мне instagram', 'instagram']
stop = ['стоп', 'пока', 'давай', 'заткнись', 'прощай']


def voice(words):
    print(words)
    egine = pyttsx3.init()
    egine.say(words)
    egine.runAndWait()


voice("Привет, чем я могу тебе помочь мой господин")


def comand():
    r = speech_recognition.Recognizer()
    with speech_recognition.Microphone(device_index=1) as microphone:
        voice("говорите, сударь")
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(microphone, duration=2)
        audio = r.listen(microphone)

    try:
        audio_comand = r.recognize_google(audio, language="ru-RU").lower()
        print("Вы сказали: " + audio_comand)
    except speech_recognition.UnknownValueError:
        voice("Не поняла")
        audio_comand = comand()

    return audio_comand


def test_1(audio_comand):
    for i in range(len(youtube)):
        if youtube[i] == audio_comand:
            voice("секунду")
            url = 'https://www.youtube.com/'
            webbrowser.open(url)


def test2(audio_comand):
    for i in range(len(instagram)):
        if instagram[i] == audio_comand:
            voice("секунду")
            url = 'https://www.instagram.com/?utm_referrer=https%253A%252F%252Fyandex.ru%252F%253Ffrom%253Dalice'
            webbrowser.open(url)


def test3(audio_comand):
    for i in range(len(stop)):
        if stop[i] == audio_comand:
            voice("До скорый встреч")
            sys.exit()


def test4(audio_comand):
    for _ in range(len(time)):
        if time[_] == audio_comand:
            now = datetime.datetime.now()
            voice("Сейчас " + str(now.hour) + ":" + str(now.minute))

#def test5(au)
#    if 'погода' == audio_comand:
#        config = cfg.get_default_config()
#        config['language'] = 'ru'
#        place = 'Казань'
#        owm = OWM('2302a1072316c2a336ceb0accc9265c9', config)
#        mgr = owm.weather_manager()
#        observation = mgr.weather_at_place(place)
#        w = observation.weather
#        print(w)


while True:
    test_1(comand())
    test4(comand())
    test3(comand())
    test2(comand())
